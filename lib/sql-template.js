var sql = `use $database-name
go

-- Alter a tablas
alter table $nombre-database-ext.dbo.$nombre-table-ext add CompanyId bigint null
alter table $nombre-database-ext.dbo.$nombre-table-ext add IndustryId bigint null
alter table $nombre-database-ext.dbo.$nombre-table-ext add PersonId bigint null
alter table $nombre-database-ext.dbo.$nombre-table-ext add experienceid bigint null
alter table $nombre-database-ext.dbo.$nombre-table-ext add PersonEmailId bigint null
alter table $nombre-database-ext.dbo.$nombre-table-ext add EmailValidationResultId bigint null
go

BEGIN TRY
	
	BEGIN TRANSACTION

DECLARE @nombreCampaign as VARCHAR(MAX) = '$nombre-campaign'

DECLARE @nextSourceId as BIGINT
select top 1 @nextSourceId = sourceID from dbo.Sources order by sourceID desc
set @nextSourceID = @nextSourceID + 1

DECLARE @sourceIDP as VARCHAR(MAX) = CONCAT('p.SourceId = ', @nextSourceID)

-- Si es un nombre gringo probablemente no sea real
EXEC [utils].[sp_copy_Campaign]
	@CampaignName = @nombreCampaign, /* Nombre de la campaña para la DB */ 
	@OriginalCampaignId = $original-campaign-id, /* Campaña a partir de la cual copiar */
	@CampaignOwnerId = $campaign-owner-id, /* Consultarlo (me lo deberían dar) */			
	@CampaignOwnerName = NULL, /* Nulo si la estoy copiando */
	@New_DailyTarget = $daily-target, /*cantidad de correos por día*/
	@New_RequiresSqlFilterClass2 = 1, /*le vamos a mandar un filtro adicional = 1*/
	@New_AdditionalFilter = @sourceIDP, /*Filtro de rateo de personas*/
	@New_SenderName = '$sender-name', /*nombre de la persona*/
	@New_Role = '$sender-role', /*role de la persona para la firma*/
	@New_EmailAccount = '$sender-email',
	@New_Domain = '$sender-domain', /*del mail*/
	@New_EmailTemplateGroupId = $email-template-group-id, /*numero de template del mail que se manda*/
	@RelatedCampaignId = NULL, /* Nulo por default */
	-- Nuevos parametros requeridos por EmailSenderSignaturesData
	@Mobile = $sender-mobile, /* Tampoco */
	@Phone = $sender-phone, /* Fijo para BairesDev */
	@Skype = $sender-skype,
	@LinkedInId = NULL,
	@Address = $sender-address /* Fijo para BairesDev */

DECLARE @campaignID bigint
select top 1 @campaignID = campaignId from campaign.Campaigns where name = @nombreCampaign order by campaignId Desc
update automation.EmailGeneration set MinRating = 0 /* Mando a todos */, DaysLastSentSameOwner = 0, DaysLastSentDifferentOwner = 0 where CampaignID = @campaignID /* Desactivo la automatizacion para crearlo, suele hacerse siempre y al final se reactiva para que se envíen de forma automática */

-- Acá importo las tablas a $nombre-database-ext (a manopla, usando import data. Si son muchos CSV elijo la misma tabla)

-- Eliminar duplicados para el mismo mail. En el order by escoges la condición de desempate
;with duplicados_cte as (
	select Email, rn = ROW_NUMBER() OVER (Partition by Email order by $firstName)
	from $nombre-database-ext.dbo.$nombre-table-ext
)
delete from duplicados_cte where rn>1;

-- Crear Source
insert into dbo.Sources (SourceId, ServiceName, Description) values (@nextSourceID, @nombreCampaign, '$descripcion-camp')

-- Setear IndustryId (ver si puedes hacer match por nombre) --Pedir nombre del campo de industria en el wizard
;with IndustriesCTE as
	(
		SELECT distinct $industry
		from 
		$nombre-database-ext.dbo.$nombre-table-ext where $industry is not null
	),
IndustriesIdCTE as
	(
		select l.$industry, i.IndustryId
		FROM dbo.Industries i (nolock) 
		JOIN IndustriesCTE l ON l.$industry = i.description
	)
UPDATE list SET list.$industry=cte.IndustryId
FROM $nombre-database-ext.dbo.$nombre-table-ext list JOIN
IndustriesIdCTE cte ON list.$industry = cte.$industry

--- Crear nuevas compañias --Pedir los nombres de campos $company-name y Website en el wizard
;with CompaniesCTE as
(
	SELECT distinct $company-name, 
	case when $website is not null then $website 
	else concat('http://www.', substring(EMAIL, charindex( '@', EMAIL) + 1, 500))
	end as website_comp,
	IndustryId,
	concat(
		case when City is not null then concat(City,',') else '' end,
		case when State is not null then concat(State,',') else '' end,
		case when Country is not null then concat(Country,',') else '' end
	) as GeographicArea,
	(select CountryId from dbo.Countries (nolock) where Description = Country) as CountryId,
	row = ROW_NUMBER() OVER (Partition by $company-name order by IndustryId desc) 
	from 
	$nombre-database-ext.dbo.$nombre-table-ext where CompanyId is null and $company-name is not null
)
insert into dbo.companies (name, $website, industryid, geographicArea, countryid, sourceId)
select $company-name, website_comp, IndustryId, GeographicArea, 1, @nextSourceID
FROM CompaniesCTE where row = 1

;with CompaniesCTE as
	(
		SELECT distinct $company-name
		from 
		$nombre-database-ext.dbo.$nombre-table-ext where CompanyId is null and $company-name is not null
	),
CompanyIdCTE as
	(
		select l.$company-name, c.CompanyId
		FROM dbo.Companies c (nolock) 
		JOIN CompaniesCTE l ON l.$company-name = c.Name and
		c.CreationDate > GETDATE() - 0.2
	)
UPDATE list
SET list.CompanyId=cte.CompanyId
FROM
$nombre-database-ext.dbo.$nombre-table-ext list JOIN
CompanyIdCTE cte ON list.$company-name = cte.$company-name

-- Crear Persons -- (pedir nombres de las tablas $role, state, first y last name, $linkedin
insert into dbo.persons (name, lastname, shorttitle, currentrole, geographicarea, email, industryid, countryid, sourceid, lastdownloaddate, publicprofile, profileId, guessedInProfileId)
select 
$firstName, 
$lastName,
concat($role, ' at ', $company-name), 
concat($role, ' at ', $company-name),
concat(isnull(isnull($city, $state), '-'), ', $pais'), 
Email, 
IndustryId, 
1, 
@nextSourceID, 
(getDate() - 180),
$linkedin,
case when $linkedin is not null then substring($linkedin, charindex('/in/', $linkedin), 999) else NULL end,
case when $linkedin is not null then substring($linkedin, charindex('/in/', $linkedin), 999) else NULL end
from $nombre-database-ext.dbo.$nombre-table-ext
where PersonId is null and $role != '' and $company-name != '' and $role is not null and $company-name is not null

update list 
set list.personid = p.PersonId
FROM $nombre-database-ext.dbo.$nombre-table-ext list
JOIN dbo.Persons p (nolock) ON list.email=p.email
and p.sourceid = @nextSourceID
where p.creationdate > getdate()-0.1 and list.PersonId is null

-- Experiences --Usar campo $role y eso de arriba
insert into dbo.experiences (personid, companyid, title, StartDate, sourceid, IsCurrent)
select personid, companyid, $role, GETDATE() - 990, @nextSourceID, 1
from $nombre-database-ext.dbo.$nombre-table-ext
where companyId is not null and $role is not null -- 12637

update list
SET list.experienceId = e.ExperienceId
FROM
$nombre-database-ext.dbo.$nombre-table-ext list JOIN
dbo.experiences e (nolock) ON e.PersonId = list.PersonId and e.CompanyId = list.CompanyId
where creationdate > getdate()-0.01 and SourceId = @nextSourceID -- 12637

update experience
set experience.IsCurrent = 1
from dbo.Experiences experience inner join
$nombre-database-ext.dbo.$nombre-table-ext campaign on experience.ExperienceId = campaign.ExperienceId

-- Mails
insert into dbo.personemail
(personid, companyid, email, emailconfidence, creationdate, updatedate)
select personid, companyid, Email, 1, getdate(), getdate()
from $nombre-database-ext.dbo.$nombre-table-ext

update list
SET list.personemailid = p.personemailid
FROM
$nombre-database-ext.dbo.$nombre-table-ext list (nolock)
JOIN dbo.personemail p (nolock) ON p.email = list.Email and list.PersonId = p.PersonId
where p.creationdate > getdate()-0.2

-- Se crean los registros en EmailQueue como procesados por el ProcessId 14
insert into validation.EmailValidationQueue 
(Email, Priority, EmailValidationProcessId, EmailValidationStatusId, EmailValidationSourceId, ValidationDate, ProcessedDate, CreationDate, UpdateDate, EmailValidationReasonId)
SELECT l.Email, 1, 14, 1, 1, GETDATE(), GETDATE(), GETDATE(), GETDATE(), 7
FROM $nombre-database-ext.dbo.$nombre-table-ext l

-- Se actualizan o crean registros en EmailValidationResults como validos --Uso $firstName del wizard
update evr
set evr.EmailValidationStatusId = 1,
    evr.EmailValidationProcessId = 14,
	evr.ValidationDate = GETDATE(),
	evr.UpdateDate = GETDATE(),
	evr.EmailValidationReasonId = 7
from validation.EmailValidationResults evr
inner join $nombre-database-ext.dbo.$nombre-table-ext l on l.Email = evr.Email

;with avoidDuplicatesCTE as
(
SELECT l.Email, row = ROW_NUMBER() OVER (Partition by l.Email order by l.$firstName asc)
from 
$nombre-database-ext.dbo.$nombre-table-ext l
WHERE NOT EXISTS (select top 1 1 from validation.EmailValidationResults (nolock) where Email = l.Email)
)
insert into validation.EmailValidationResults 
(Email, EmailValidationStatusId, EmailValidationProcessId, ValidationDate, CreationDate, UpdateDate, EmailValidationReasonId)
SELECT cte.Email, 1, 14, GETDATE(), GETDATE(), GETDATE(), 7
FROM avoidDuplicatesCTE cte 
WHERE row = 1

update list
SET list.EmailValidationResultId = p.EmailValidationResultId
FROM
$nombre-database-ext.dbo.$nombre-table-ext list (nolock)
JOIN validation.EmailValidationResults p (nolock) ON p.email = list.Email
where p.updateDate > getdate()-0.2 and p.EmailValidationProcessId = 14

;with avoidDuplicatesCTE as
(
SELECT l.EmailValidationResultId, row = ROW_NUMBER() OVER (Partition by l.EmailValidationResultId order by l.$firstName asc)
from 
$nombre-database-ext.dbo.$nombre-table-ext l
WHERE EXISTS (select top 1 1 from validation.EmailValidationResults (nolock) where Email = l.Email and EmailValidationProcessId = 14)
)
insert into validation.EmailValidationResultsHistory 
(EmailValidationResultId, EmailValidationStatusId, EmailValidationProcessId, ValidationDate, CreationDate, UpdateDate, EmailValidationReasonId)
SELECT cte.EmailValidationResultId, 1, 14, GETDATE(), GETDATE(), GETDATE(), 7
FROM avoidDuplicatesCTE cte
WHERE row = 1

-- Actualizar campaña
DECLARE @SqlFilterId BIGINT
select @SqlFilterId = sqlFilterId from [campaign].SqlFilters where campaignId = @campaignID
update [campaign].StatementSqlFilterStatements set SqlStatement = concat('p.SourceId is not null and p.SourceId = ', @nextSourceId) where SqlFilterId = @SqlFilterId

-- Ratear personas para esta campaña (asíncrono)
DECLARE @rating INT = $rating
insert into campaign.CampaignRatedPersons (CampaignId, Rating, PersonId, CompanyId, ExperienceId, deprecated_processed, emailInferencesCount, deprecated_active, deprecated_reparse, CreationDate, UpdateDate)
select @campaignId, @rating, PersonId, CompanyId, ExperienceId, 1, 0, 1, NULL, GETDATE(), GETDATE()
from $nombre-database-ext.dbo.$nombre-table-ext
where PersonId is not null and CompanyId is not null and ExperienceId is not null

-- Crear potentialLeads
insert into campaign.CampaignPotentialLeads (campaignratedPersonId, CampaignId, PersonId, CompanyId, ExperienceId, Rating, EmailPersonId, deprecated_hasBeenProcessed, CampaignPotentialLeadStatusId, CreationDate, UpdateDate)
select crp.CampaignRatedPersonId, @campaignId, crp.PersonId, crp.CompanyId, crp.ExperienceId, crp.Rating, u.PersonEmailId, 1, NULL, GETDATE(), GETDATE()
from $nombre-database-ext.dbo.$nombre-table-ext u inner join
$database-name.campaign.CampaignRatedPersons crp on (crp.PersonId = u.PersonId and crp.ExperienceId = u.ExperienceId and crp.CompanyId = u.CompanyId and crp.CampaignId = @campaignId)

-- Encolar para envio de mails
declare @list [dbo].[BigIntList]
insert into @list (Item)
select campaignPotentialLeadId from $database-name.campaign.campaignPotentialLeads (nolock) where campaignId = @campaignID and creationdate > GETDATE() - 2
exec [utils].[sp_AdvertisingLeadsFromList] @list
print concat('El sourceID de esta campaña fue: ', @nextSourceId)
print concat('El id de la campaña fue: ', @campaignID)

IF (@@TRANCOUNT > 0)
	COMMIT

END TRY
BEGIN CATCH

		DECLARE @Message nvarchar(300)
			
		SELECT @Message = CONCAT('Error Number: ',  ERROR_NUMBER(),   
		', Error Severity: ',ERROR_SEVERITY(),  
		', Error State: ',ERROR_STATE(),
		', Error Procedure: ',ERROR_PROCEDURE(),  
		', Error Line: ',ERROR_LINE(),
		', Error Message: ',ERROR_MESSAGE());

		IF (@@TRANCOUNT > 0)
			ROLLBACK TRANSACTION;

		INSERT INTO LOGS (Category, IP, Message) VALUES (1988, @@SERVERNAME, 'Campaign adhoc creation calls rollback with message: ' + @Message);

		THROW 50001, @Message, 1
  
END CATCH
`
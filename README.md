# foca
**foca** (*Formulario de Origen de Campañas Adhoc*) es un wizard para 
crear campañas adhoc. Consiste de un formulario que realiza una serie de 
preguntas a partir de las cuales genera un query SQL que puede usarse 
para generar una campaña adhoc con un solo click. Explica también cómo 
importar los datos a la base de datos y cómo testear que todo haya 
funcionado correctamente.

![screenshot.png](screenshot.png)
